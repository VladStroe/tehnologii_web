const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
const app = express();
const port = 3001;
const apiKey = "137388c302c4c7c6c402102c02fb4723";
const apiDomain = 'https://api.themoviedb.org/3/';

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/',(req,res)=>res.send("Hello World!"));

app.post('/test',function (req,res){
    res.send("Api is working");
});

app.get('/top-rated-movies',function(req,res){
    if(req.query.page){
        axios.get(apiDomain+'movie/top_rated?api_key='+apiKey+"&page="+req.query.page).then((response)=>{
            res.send(response.data);
        }).catch((error)=>{
            console.log(error);
        })
    }else{
        axios.get(apiDomain+'movie/top_rated?api_key='+apiKey).then((response)=>{
            res.send(response.data);
        }).catch((error)=>{
            console.log(error);
        })
    }
});

app.listen(port,()=>console.log(`Example app is running on port: ${port}`));
