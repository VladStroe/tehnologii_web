import React, {Component} from 'react';

class Movie extends Component{

    render(){

        return (
            <div className="col-12 col-md-4 my-2">
                <div className="card">
                    <img src={'https://image.tmdb.org/t/p/w500/'+this.props.movie.poster_path} className="card-img-top img-fluid" alt="..." />
                    <div className="card-body">
                        <h3 className="font-weight-bold">{this.props.movie.title}</h3>
                        <p>{this.props.movie.overview}</p>
                    </div>
                    <div className="card-footer d-flex justify-content-around">
                            <p className="align-middle my-auto">Popularity: {this.props.movie.popularity}</p>
                            <button className="btn btn-primary btn-sm align-middle my-auto">Add to list</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Movie;
