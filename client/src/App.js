import React, { Component } from 'react';
import Movie from './Components/Movie';
// import './main.scss';

class App extends Component {

  constructor(props){
    super(props);

    this.state =  {
      top_rated_movies:[],
      page: 1
    };

    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);

  }

  componentDidMount(){
    fetch("http://localhost:3001/top-rated-movies?page="+this.state.page,{
      crossDomain:true,
      method:"GET",
      headers: {'Content-Type':'application/json'},
    }).then(res => res.text()).then((res)=>{
      let results = JSON.parse(res);
      console.log(results);
      this.setState({top_rated_movies:results.results,page:results.page});
    });
  };
  render() {

    let isFirstPage = this.state.page === 1;

    return (
      <div className="App">
        <header className="App-header my-4">
          <h1 className="text-center">My movie database</h1>
        </header>


        <div className="container">
          <div className="row">
            {this.state.top_rated_movies.map((movie,index)=><Movie key={index} movie={movie}/>)}
          </div>
          <div className="row my-4">
            <div className="col-12 d-flex justify-content-end">
              {isFirstPage === false &&
               <button className="mx-1" onClick={this.previousPage}>Previous</button>
              }

              <button className="mx-1" onClick={this.nextPage}>Next</button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  getMovies(page){
    fetch("http://localhost:3001/top-rated-movies?page="+page,{
      crossDomain:true,
      method:"GET",
      headers: {'Content-Type':'application/json'},
    }).then(res => res.text()).then((res)=>{
      let results = JSON.parse(res);
      console.log(results);
      this.setState({top_rated_movies:results.results,page:results.page});
    });
  }

  nextPage(){
    let nextPage = this.state.page + 1;
    this.getMovies(nextPage);
    this.setState({page:nextPage});
  }

  previousPage(){
    let previousPage = this.state.page - 1;
    this.getMovies(previousPage);
    this.setState({page:previousPage});
  }
}

export default App;
